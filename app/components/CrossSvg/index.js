/**
 *
 * CrossSvg
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

const CrossSvg = props => (
  <svg
    width={100}
    height={100}
    viewBox="0 0 50 50"
    className="icon-cross"
    {...props}
  >
    <path
      fill={props.fill}
      d="M17.8 19.2L15 16.4l-2.8 2.8-1.4-1.4 2.8-2.8-2.8-2.8 1.4-1.4 2.8 2.8 2.8-2.8 1.4 1.4-2.8 2.8 2.8 2.8-1.4 1.4z"
    />
  </svg>
);
CrossSvg.defaultProps = {
  fill: '#fff',
};
CrossSvg.propTypes = {
  fill: PropTypes.string,
};

export default CrossSvg;
