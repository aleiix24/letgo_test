/**
 *
 * Lightbox
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Img from 'components/Img';
import ArrowLeftSvg from 'components/ArrowLeftSvg';
import ArrowRightSvg from 'components/ArrowRightSvg';
import CrossSvg from 'components/CrossSvg';
import {
  Wrapper,
  ImgWrapper,
  Username,
  Title,
  AuthorInfo,
  AuthorMainInfo,
  AuthorLocation,
  AuthorDescription,
  Button,
  Separator,
} from './Elements';

/* eslint-disable react/prefer-stateless-function */
class Lightbox extends React.Component {
  render() {
    const { onPrev, onNext, onClose, ...rest } = this.props;
    return (
      <Wrapper>
        <Button onClick={() => onClose()} direction="right">
          <CrossSvg />
        </Button>
        <Button onClick={() => onPrev()} direction="left" align>
          <ArrowLeftSvg />
        </Button>
        <ImgWrapper>
          <Title>{this.props.title}</Title>
          <Img {...rest} size="b" />

          {this.props.user && (
            <AuthorInfo>
              <AuthorMainInfo>
                <Username>{this.props.user.username.content}</Username>
                {this.props.user.location && (
                  <React.Fragment>
                    <Separator>-</Separator>
                    <AuthorLocation>
                      {this.props.user.location.content}
                    </AuthorLocation>
                  </React.Fragment>
                )}
              </AuthorMainInfo>
              {this.props.user.description && (
                <AuthorDescription
                  dangerouslySetInnerHTML={{
                    __html: this.props.user.description.content,
                  }}
                />
              )}
            </AuthorInfo>
          )}
        </ImgWrapper>
        <Button onClick={() => onNext()} direction="right" align>
          <ArrowRightSvg />
        </Button>
      </Wrapper>
    );
  }
}

Lightbox.propTypes = {
  id: PropTypes.string.isRequired,
  owner: PropTypes.string.isRequired,
  user: PropTypes.object,
  title: PropTypes.string,
  onNext: PropTypes.func.isRequired,
  onPrev: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Lightbox;
