/**
 *
 * Button
 *
 */

import styled from 'styled-components';

const ButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
  flex-direction: row;
  margin: 20px;
`;
const ButtonElement = styled.button`
  margin: auto;
  width: 100%;
  max-width: 250px;
  padding: 15px;
  color: #333;
  background: #fff;
  border: 1px solid #333;
  display: block;
  cursor: pointer;
  ${props => props.disabled && 'opacity: .2'};
`;

export { ButtonWrapper };
export default ButtonElement;
