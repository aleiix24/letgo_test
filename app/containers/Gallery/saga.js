/**
 * Gets the images from the API
 */

import { put, takeLatest, select, call } from 'redux-saga/effects';
import request from 'utils/request';
import { API_PATH } from 'utils/constants';
import overrideKey from 'utils/overrideKey';
import {
  makeSelectPage,
  makeSelectItemsPage,
  makeSelectUserId,
  makeSelectImageId,
} from './selectors';
import {
  IMAGES_REQUEST,
  IMAGES_REQUEST_SUCCESS,
  IMAGES_REQUEST_ERROR,
  USER_REQUEST,
  USER_REQUEST_SUCCESS,
  USER_REQUEST_ERROR,
} from './constants';

function* getImages() {
  const page = yield select(makeSelectPage());
  const itemsPage = yield select(makeSelectItemsPage());

  try {
    const requestUrl = `${API_PATH}/getImages?page=${page}&itemsPage=${itemsPage}`;
    const { response } = yield call(request, requestUrl);

    yield put({
      type: IMAGES_REQUEST_SUCCESS,
      images: response.photos.photo,
    });
    return response;
  } catch (err) {
    yield put({
      type: IMAGES_REQUEST_ERROR,
      error: {
        message: 'Some error occur',
        status: 403,
      },
    });
    return err;
  }
}

function* getUser() {
  const userId = yield select(makeSelectUserId());
  const imageId = yield select(makeSelectImageId());
  try {
    const requestUrl = `${API_PATH}/getUser/${userId}`;
    const { response } = yield call(request, requestUrl);
    const user = overrideKey(response.person, '_content', 'content');

    yield put({
      type: USER_REQUEST_SUCCESS,
      user,
      imageId,
    });
    return user;
  } catch (err) {
    console.log(err);
    yield put({
      type: USER_REQUEST_ERROR,
      error: {
        message: 'Some error occur',
        status: 403,
      },
    });
    return err;
  }
}

export default function* root() {
  yield takeLatest(IMAGES_REQUEST, getImages);
  yield takeLatest(USER_REQUEST, getUser);
}
