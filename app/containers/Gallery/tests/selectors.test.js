import { fromJS } from 'immutable';
import { makeSelectImages } from '../selectors';

describe('selectGalleryDomain', () => {
  describe('makeSelectImages', () => {
    const selector = makeSelectImages();
    it('should select the images', () => {
      const images = [{ image: 'testImage' }];
      const mockedState = fromJS({
        gallery: {
          images,
        },
      });
      expect(selector(mockedState).toJS()).toEqual(images);
    });
  });
});
