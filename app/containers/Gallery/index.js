/**
 *
 * Gallery
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import Masonry from 'react-masonry-component';
import Loading from 'components/Loading';
import Button from 'components/Button';
import Lightbox from 'components/Lightbox';
import GalleryItem from 'components/GalleryItem';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import VisibilitySensor from 'react-visibility-sensor';
import {
  makeSelectImages,
  makeSelectLoading,
  makeSelectError,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import { retrieveImages, retrieveUser } from './actions';
import Wrapper from './Wrapper';

/* eslint-disable react/prefer-stateless-function, react/no-did-update-set-state */
export class Gallery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      itemsPage: 20,
      lightbox: false,
    };
  }
  componentDidMount() {
    this.props.getImages(this.state);
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.lightbox && !this.state.lightbox.user) {
      const images = this.props.images.toJS();
      for (let index = 0; index < images.length; index += 1) {
        if (images[index].id === this.state.lightbox.id && images[index].user) {
          this.setState({ lightbox: { ...images[index] } });
        }
      }
    }
    if (prevState.page !== this.state.page) {
      this.props.getImages(this.state);
    }
  }
  handleMouseEnter(userId, imageId) {
    this.props.getUser(userId, imageId);
  }
  handleClick(imageProps) {
    if (!imageProps.user) {
      this.props.getUser(imageProps.owner, imageProps.id);
    }
    this.setState({ lightbox: { ...imageProps } });
  }
  handleNext() {
    const images = this.props.images.toJS();
    let next;
    for (let index = 0; index < images.length; index += 1) {
      if (images[index].id === this.state.lightbox.id) {
        next = index + 1;
      }
    }
    if (images[next]) {
      if (!images[next].user) {
        this.props.getUser(images[next].owner, images[next].id);
      }
      this.setState({ lightbox: { ...images[next] } });
    }
  }
  handlePrev() {
    const images = this.props.images.toJS();
    let prev = false;
    for (let index = 0; index < images.length; index += 1) {
      if (images[index].id === this.state.lightbox.id) {
        prev = index - 1;
      }
    }
    if (images[prev]) {
      if (!images[prev].user) {
        this.props.getUser(images[prev].owner, images[prev].id);
      }
      this.setState({ lightbox: { ...images[prev] } });
    }
  }
  handleClose() {
    this.setState({ lightbox: false });
  }
  loadMore() {
    this.setState({ page: this.state.page + 1 });
  }
  renderItem(image) {
    return (
      <GalleryItem
        title={image.title}
        farm={image.farm}
        server={image.server}
        id={image.id}
        secret={image.secret}
        key={image.id}
        owner={image.owner}
        user={image.user}
        onMouseEnter={(userId, imageId) =>
          this.handleMouseEnter(userId, imageId)
        }
        onClick={imageProps => this.handleClick(imageProps)}
      />
    );
  }
  render() {
    const images = this.props.images.toJS();
    if (this.props.error) {
      return <h2>{this.props.error.message}</h2>;
    }
    return (
      <Wrapper>
        {images.length > 0 && (
          <Masonry>{images.map(image => this.renderItem(image))}</Masonry>
        )}
        {!this.props.loading && (
          <VisibilitySensor
            onChange={isVisible => isVisible && this.loadMore()}
            minTopValue={300}
            delayedCall
          >
            <Button
              onClick={() => this.loadMore()}
              disabled={this.props.loading}
            >
              Load More
            </Button>
          </VisibilitySensor>
        )}
        {this.props.loading && <Loading />}
        {this.state.lightbox && (
          <Lightbox
            {...this.state.lightbox}
            onNext={() => this.handleNext()}
            onPrev={() => this.handlePrev()}
            onClose={() => this.handleClose()}
          />
        )}
      </Wrapper>
    );
  }
}

Gallery.propTypes = {
  getImages: PropTypes.func,
  getUser: PropTypes.func,
  images: PropTypes.object,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  loading: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  images: makeSelectImages(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

function mapDispatchToProps(dispatch) {
  return {
    getImages: (page, itemsPage) => dispatch(retrieveImages(page, itemsPage)),
    getUser: (userId, imageId) => dispatch(retrieveUser(userId, imageId)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = injectReducer({ key: 'gallery', reducer });
const withSaga = injectSaga({ key: 'gallery', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Gallery);
