/*
 *
 * Gallery reducer
 *
 */

import { fromJS } from 'immutable';
import {
  IMAGES_REQUEST,
  IMAGES_REQUEST_SUCCESS,
  IMAGES_REQUEST_ERROR,
  USER_REQUEST,
  USER_REQUEST_SUCCESS,
} from './constants';

export const initialState = fromJS({
  images: fromJS([]),
  loading: false,
  error: false,
});

const imagesWithUser = (state, action) =>
  state.toJS().images.map(image => {
    if (image.id === action.imageId) {
      return Object.assign(image, { user: action.user });
    }
    return image;
  });

function galleryReducer(state = initialState, action) {
  switch (action.type) {
    case IMAGES_REQUEST:
      return state
        .set('page', action.page)
        .set('itemsPage', action.itemsPage)
        .set('error', false)
        .set('loading', true);
    case IMAGES_REQUEST_ERROR:
      return state.set('error', action.error).set('loading', false);
    case IMAGES_REQUEST_SUCCESS:
      return state
        .update('images', images => images.concat(action.images))
        .set('error', action.error)
        .set('loading', false);
    case USER_REQUEST:
      return state.set('userId', action.userId).set('imageId', action.imageId);
    case USER_REQUEST_SUCCESS:
      return state.set('images', fromJS(imagesWithUser(state, action)));
    default:
      return state;
  }
}

export default galleryReducer;
