/**
 *
 * Item
 *
 */
import styled from 'styled-components';

const Info = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.75);
  opacity: 0;
  transition: opacity 0.5s ease-in-out;
  border-radius: 8px;
`;
/* stylelint-disable selector-type-no-unknown  */
const Wrapper = styled.div`
  width: 100%;
  padding-bottom: 28px;
  padding-left: 12px;
  padding-right: 12px;
  border-radius: 8px;

  img {
    border-radius: 8px;
  }

  @media (min-width: 420px) {
    width: 50%;
  }
  @media (min-width: 900px) {
    width: 33.333%;
  }
  @media (min-width: 1400px) {
    width: 25%;
  }
`;
const InnerWrapper = styled.div`
  position: relative;
  display: flex;
  cursor: zoom-in;

  &:hover ${Info} {
    opacity: 1;
  }

  img {
    width: 100%;
  }
`;
const Title = styled.h4`
  font-family: Arial, sans-serif;
  color: #fff;
  font-weight: 300;
  font-size: 21px;
  border-bottom: 1px solid #fff;
  position: absolute;
  top: 20%;
  text-align: center;
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0 10px;
`;
const Author = styled.span`
  font-family: Arial, sans-serif;
  color: #ddc07f;
  font-weight: 400;
  font-size: 14px;
  text-align: center;
  display: block;
  position: absolute;
  width: 100%;
  top: 50%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0 5px;
`;

export { Wrapper, InnerWrapper, Info, Title, Author };
