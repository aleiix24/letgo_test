/**
 *
 * Button
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import ButtonElement, { ButtonWrapper } from './Button';

function Button(props) {
  return (
    <ButtonWrapper>
      <ButtonElement
        disabled={props.disabled}
        onClick={() => !props.disabled && props.onClick()}
      >
        {props.children}
      </ButtonElement>
    </ButtonWrapper>
  );
}

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  children: PropTypes.string,
};

export default Button;
