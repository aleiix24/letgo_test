/**
 * Remove all specified keys from an object, no matter how deep they are.
 * The removal is done in place, so run it on a copy if you don't want to modify the original object.
 * This function has no limit so circular objects will probably crash the browser
 *
 * @param obj The object from where you want to remove the keys
 * @param key An array of property names (strings) to remove
 * @param newkey An array of property names (strings) to remove
 */
/* eslint-disable no-restricted-syntax, default-case, no-prototype-builtins, no-param-reassign */
const overrideKey = (obj, key, newkey) => {
  let index;
  for (const prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      switch (typeof obj[prop]) {
        case 'string':
          index = key.indexOf(prop);
          if (index > -1) {
            obj[newkey] = obj[prop];
            delete obj[prop];
          }
          break;
        case 'object':
          index = key.indexOf(prop);
          if (index > -1) {
            obj[newkey] = obj[prop];
            delete obj[prop];
          } else {
            overrideKey(obj[prop], key, newkey);
          }
          break;
      }
    }
  }
  return obj;
};

export default overrideKey;
