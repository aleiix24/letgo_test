/**
 *
 * GalleryItem
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Img from 'components/Img';
import { Wrapper, InnerWrapper, Info, Author, Title } from './Item';

/* eslint-disable react/prefer-stateless-function */
class GalleryItem extends React.Component {
  render() {
    const { onMouseEnter, onClick, ...rest } = this.props;
    return (
      <Wrapper>
        <InnerWrapper
          onClick={() => onClick(rest)}
          onMouseEnter={() => onMouseEnter(this.props.owner, this.props.id)}
        >
          <Img {...rest} />
          <Info>
            <Title>{this.props.title}</Title>
            <Author>
              {this.props.user && `@${this.props.user.username.content}`}
            </Author>
          </Info>
        </InnerWrapper>
      </Wrapper>
    );
  }
}

GalleryItem.propTypes = {
  onMouseEnter: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  owner: PropTypes.string.isRequired,
  user: PropTypes.object,
  title: PropTypes.string,
};

export default GalleryItem;
