/**
 *
 * Header
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const HeaderWrapper = styled.div`
  padding: 2em;
  display: flex;
  flex-direction: row;
  flex: 1;
`;

function Header(props) {
  return <HeaderWrapper>{React.Children.only(props.children)}</HeaderWrapper>;
}

Header.propTypes = {
  children: PropTypes.object.isRequired,
};

export default Header;
