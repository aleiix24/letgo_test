/*
 *
 * Gallery constants
 *
 */

export const IMAGES_REQUEST = 'app/Gallery/IMAGES_REQUEST';
export const IMAGES_REQUEST_SUCCESS = 'app/Gallery/IMAGES_REQUEST_SUCCESS';
export const IMAGES_REQUEST_ERROR = 'app/Gallery/IMAGES_REQUEST_ERROR';
export const USER_REQUEST = 'app/Gallery/USER_REQUEST';
export const USER_REQUEST_SUCCESS = 'app/Gallery/USER_REQUEST_SUCCESS';
export const USER_REQUEST_ERROR = 'app/Gallery/USER_REQUEST_ERROR';
