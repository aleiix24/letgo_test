/**
 *
 * Elements
 *
 */
import styled from 'styled-components';

/* stylelint-disable property-no-unknown */
const Button = styled.button`
  position: absolute;
  top: ${props => (props.align ? '45%' : '0')};
  width: 50px;
  z-index: 9999;
  cursor: pointer;
  outline: 0;

  ${props => props.direction}: 10px;
`;
const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.8);
  outline: none;
  z-index: 1000;
  touch-action: none;
`;
const ImgWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  overflow: scroll;
  margin: auto;
  height: 100%;
  flex-flow: wrap;

  > img {
    max-height: 65%;
    width: auto;
    margin: 25px;
  }
`;
const Title = styled.h4`
  font-family: Arial, sans-serif;
  color: #fff;
  font-weight: 300;
  font-size: 21px;
  border-bottom: 1px solid #fff;
  text-align: center;
  width: 100%;
  padding: 0 10px 25px;
  margin-top: 25px;
  margin-bottom: auto;
`;
const AuthorMainInfo = styled.div`
  display: block;
  text-align: center;
  padding-bottom: 20px;
`;
const Username = styled.span`
  font-family: Arial, sans-serif;
  color: #fff;
  font-weight: 400;
  font-size: 14px;
`;
const AuthorLocation = Username.extend`
  font-size: 12px;
  text-decoration: underline;
`;
const AuthorDescription = Username.extend`
  font-size: 12px;

  a {
    color: #fff;
  }
`;
const AuthorInfo = styled.div`
  margin-bottom: 0;
  margin-top: auto;
  background: #000;
  padding: 25px;
  overflow: hidden;
  width: 100%;
`;
const Separator = Username.extend`
  display: inline;
  color: #fff;
  margin: 0 5px;
`;

export {
  Wrapper,
  ImgWrapper,
  Title,
  Username,
  AuthorInfo,
  AuthorMainInfo,
  AuthorLocation,
  AuthorDescription,
  Button,
  Separator,
};
