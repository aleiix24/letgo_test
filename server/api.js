const Flickr = require('flickrapi');
const { flickerConfig } = require('./keys/flicker');

const Api = (function() {
  /* eslint-disable no-shadow */
  function Api() {
    this.flickr = null;

    Flickr.authenticate(flickerConfig, (error, flickr) => {
      if (error) {
        console.log(error);
        throw error;
      }
      this.flickr = flickr;
    });
  }
  /* eslint-enable no-shadow */

  Api.prototype.getImages = function(page, itemsPage) {
    return new Promise((resolve, reject) => {
      try {
        this.flickr.photos.search(
          {
            text: 'national+geographic',
            page: page || 1,
            per_page: itemsPage || 20,
          },
          (error, images) => {
            if (error) {
              console.log(error);
              reject(error);
            }
            resolve({ images });
          },
        );
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  };

  Api.prototype.getUser = function(userId) {
    return new Promise((resolve, reject) => {
      try {
        this.flickr.people.getInfo(
          {
            user_id: userId,
          },
          (error, images) => {
            if (error) {
              console.log(error);
              reject(error);
            }
            resolve({ images });
          },
        );
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  };

  return Api;
})();
module.exports = Api;
