/**
 *
 * Img
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

const createFlickrSrc = ({ farm, server, id, secret, size }) =>
  size
    ? `https://c1.staticflickr.com/${farm}/${server}/${id}_${secret}_${size}.jpg`
    : `https://c1.staticflickr.com/${farm}/${server}/${id}_${secret}.jpg`;

function Img(props) {
  return <img src={createFlickrSrc(props)} alt={props.title} />;
}

/* eslint-disable react/no-unused-prop-types */
Img.propTypes = {
  farm: PropTypes.number.isRequired,
  server: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  secret: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  size: PropTypes.string,
};
/* eslint-enable react/no-unused-prop-types */

export default Img;
