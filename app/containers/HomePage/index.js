/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import H1 from 'components/H1';
import Wrapper from 'components/Wrapper';
import Header from 'components/Header';
import Gallery from 'containers/Gallery';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.PureComponent {
  render() {
    return (
      <Wrapper>
        <Header>
          <H1>
            <FormattedMessage {...messages.header} />
          </H1>
        </Header>
        <Wrapper>
          <Gallery />
        </Wrapper>
      </Wrapper>
    );
  }
}
