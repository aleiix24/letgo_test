import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the gallery state domain
 */

const selectGalleryDomain = state => state.get('gallery', initialState);

/**
 * Other specific selectors
 */
const makeSelectImages = () =>
  createSelector(selectGalleryDomain, state => state.get('images'));

const makeSelectError = () =>
  createSelector(selectGalleryDomain, state => state.get('error'));

const makeSelectLoading = () =>
  createSelector(selectGalleryDomain, state => state.get('loading'));

const makeSelectPage = () =>
  createSelector(selectGalleryDomain, state => state.get('page'));

const makeSelectItemsPage = () =>
  createSelector(selectGalleryDomain, state => state.get('itemsPage'));

const makeSelectUserId = () =>
  createSelector(selectGalleryDomain, state => state.get('userId'));

const makeSelectImageId = () =>
  createSelector(selectGalleryDomain, state => state.get('imageId'));

/**
 * Default selector used by Gallery
 */

const makeSelectGallery = () =>
  createSelector(selectGalleryDomain, substate => substate.toJS());

export default makeSelectGallery;
export {
  makeSelectImages,
  makeSelectError,
  makeSelectLoading,
  makeSelectPage,
  makeSelectItemsPage,
  makeSelectUserId,
  makeSelectImageId,
};
