/**
 *
 * H1
 *
 */
import styled from 'styled-components';

const H1 = styled.h1`
  font-size: 34px;
  line-height: 38px;
  margin: 0;
  font-weight: 700;
  color: #333;
  font-family: Arial, sans-serif;
`;

export default H1;
