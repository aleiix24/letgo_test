import React from 'react';
import { shallow } from 'enzyme';

import Button from '../index';
import ButtonElement from '../Button';

describe('<Button />', () => {
  it('should render Wrapper element', () => {
    const props = {
      onClick: jest.fn(),
    };
    const renderedComponent = shallow(<Button {...props} />);
    expect(renderedComponent.find(ButtonElement)).toHaveLength(1);
  });
});
