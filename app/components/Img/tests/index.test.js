import React from 'react';
import { shallow } from 'enzyme';

import Img from '../index';

describe('<Img />', () => {
  it('should render a img', () => {
    const props = {
      title: 'title',
      farm: 4,
      server: '1',
      id: '1',
      secret: '1234',
    };
    const renderedComponent = shallow(<Img {...props} />);
    expect(renderedComponent.find('img').length).toEqual(1);
  });
});
