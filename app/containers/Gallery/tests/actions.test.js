import { retrieveImages } from '../actions';
import { IMAGES_REQUEST } from '../constants';

describe('Gallery actions', () => {
  describe('retrieveImages', () => {
    it('has a type of IMAGES_REQUEST', () => {
      const page = 1;
      const itemsPage = 20;
      const expected = {
        type: IMAGES_REQUEST,
        page,
        itemsPage,
      };
      expect(retrieveImages({ page, itemsPage })).toEqual(expected);
    });
  });
});
