/*
 *
 * Gallery actions
 *
 */

import { IMAGES_REQUEST, USER_REQUEST } from './constants';

export function retrieveImages({ page, itemsPage }) {
  return {
    type: IMAGES_REQUEST,
    page,
    itemsPage,
  };
}
export function retrieveUser(userId, imageId) {
  return {
    type: USER_REQUEST,
    userId,
    imageId,
  };
}
