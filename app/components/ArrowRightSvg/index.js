import React from 'react';
import PropTypes from 'prop-types';

const ArrowRightSvg = props => (
  <svg viewBox="0 0 17.02 18.14" className="arrow-right-icon" {...props}>
    <g>
      <path
        fill={props.fill}
        d="M16.74 9.54l-.16.16-8.44 8.44-.89-.89 7.56-7.55H0V8.44h14.81L7.25.89 8.14 0l8.44 8.44.16.16.28.28v.38l-.28.28z"
      />
    </g>
  </svg>
);

ArrowRightSvg.defaultProps = {
  fill: '#fff',
};

ArrowRightSvg.propTypes = {
  fill: PropTypes.string,
};

export default ArrowRightSvg;
