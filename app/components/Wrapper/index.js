/**
 *
 * Wrapper
 *
 */

import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 0.5em;
  display: flex;
  flex-direction: column;
  flex: 1;

  @media (min-width: 420px) {
    padding: 1em;
  }
  @media (min-width: 900px) {
    padding: 1.5em;
  }
  @media (min-width: 1400px) {
    padding: 2em;
  }
`;

export default Wrapper;
