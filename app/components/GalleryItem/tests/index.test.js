import React from 'react';
import { render, shallow } from 'enzyme';
import { Wrapper } from '../Item';
import GalleryItem from '../index';

describe('<GalleryItem />', () => {
  it('should render the title', () => {
    const props = {
      title: 'title',
      farm: 4,
      server: '1',
      id: '1',
      secret: '1234',
      owner: 'owner',
      onMouseEnter: jest.fn(),
      onClick: jest.fn(),
    };
    const renderedComponent = render(<GalleryItem {...props} />);
    expect(renderedComponent.text()).toMatch(/title/);
  });
  it('should render Wrapper element', () => {
    const props = {
      title: 'title',
      farm: 4,
      server: '1',
      id: '1',
      secret: '1234',
      owner: 'owner',
      onMouseEnter: jest.fn(),
      onClick: jest.fn(),
    };
    const renderedComponent = shallow(<GalleryItem {...props} />);
    expect(renderedComponent.find(Wrapper)).toHaveLength(1);
  });
});
