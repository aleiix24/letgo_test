import React from 'react';
import { shallow } from 'enzyme';
import Loading from 'components/Loading';
import { fromJS } from 'immutable';
import { Gallery } from '../index';

describe('<Gallery />', () => {
  it('should render the gallery item', () => {
    const renderedComponent = shallow(
      <Gallery
        loading
        error={false}
        images={fromJS({})}
        getImages={jest.fn()}
      />,
    );
    expect(renderedComponent.contains(<Loading />)).toEqual(true);
  });
});
