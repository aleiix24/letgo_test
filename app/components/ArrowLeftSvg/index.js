import React from 'react';
import PropTypes from 'prop-types';

const ArrowLeftSvg = props => (
  <svg viewBox="0 0 17.02 18.14" className="arrow-left-icon" {...props}>
    <g>
      <path
        d="M3.362 6.666h12.896v1.861H3.36l5.096 4.948-1.336 1.316L.668 8.527H.421v-.24L0 7.88l.286-.282L0 7.316l.421-.41v-.24H.67L7.12.403 8.458 1.72 3.362 6.666z"
        fill={props.fill}
        fillRule="evenodd"
      />
    </g>
  </svg>
);

ArrowLeftSvg.defaultProps = {
  fill: '#fff',
};

ArrowLeftSvg.propTypes = {
  fill: PropTypes.string,
};

export default ArrowLeftSvg;
