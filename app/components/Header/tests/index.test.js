import React from 'react';
import { mount, render } from 'enzyme';

import Header from '../index';

describe('<Header />', () => {
  it('should render a div', () => {
    const renderedComponent = mount(
      <Header>
        <h1>headerTitle</h1>
      </Header>,
    );
    expect(renderedComponent.find('div').length).toEqual(1);
  });
  it('should render the text', () => {
    const renderedComponent = render(
      <Header>
        <h1>headerTitle</h1>
      </Header>,
    );
    expect(renderedComponent.text()).toMatch(/headerTitle/);
  });
});
