import React from 'react';
import { shallow, render } from 'enzyme';

import Lightbox from '../index';
import { Wrapper } from '../Elements';

describe('<Lightbox />', () => {
  it('should render the title', () => {
    const props = {
      title: 'title',
      farm: 4,
      server: '1',
      id: '1',
      secret: '1234',
      owner: 'owner',
      onNext: jest.fn(),
      onPrev: jest.fn(),
      onClose: jest.fn(),
    };
    const renderedComponent = render(<Lightbox {...props} />);
    expect(renderedComponent.text()).toMatch(/title/);
  });
  it('should render Wrapper element', () => {
    const props = {
      title: 'title',
      farm: 4,
      server: '1',
      id: '1',
      secret: '1234',
      owner: 'owner',
      onNext: jest.fn(),
      onPrev: jest.fn(),
      onClose: jest.fn(),
    };
    const renderedComponent = shallow(<Lightbox {...props} />);
    expect(renderedComponent.find(Wrapper)).toHaveLength(1);
  });
  it('should render the username', () => {
    const props = {
      title: 'title',
      farm: 4,
      server: '1',
      id: '1',
      secret: '1234',
      owner: 'owner',
      onNext: jest.fn(),
      onPrev: jest.fn(),
      onClose: jest.fn(),
      user: {
        username: {
          content: 'username',
        },
      },
    };
    const renderedComponent = render(<Lightbox {...props} />);
    expect(renderedComponent.text()).toMatch(/username/);
  });
});
