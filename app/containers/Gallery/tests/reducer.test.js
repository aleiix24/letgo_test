import { fromJS } from 'immutable';
import galleryReducer from '../reducer';

describe('galleryReducer', () => {
  let state;
  beforeEach(() => {
    state = fromJS({
      images: fromJS([]),
      loading: false,
      error: false,
    });
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(galleryReducer(undefined, {})).toEqual(expectedResult);
  });
});
